# Copyright (c) 2012-2013 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2006-2008 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Steve Reinhardt

# Simple test script
#
# "m5 test.py"

from __future__ import print_function

import optparse
import sys
import os

import m5
# from m5.defines import buildEnv
from m5.objects import *
# from m5.util import addToPath, fatal, warn

m5.util.addToPath('../')
# import the caches which we made
from caches import *
from O3_X86_SC import *

from common import Options
# from common import Simulation
# from common import CacheConfig
# from common import CpuConfig
# from common import MemConfig

parser = optparse.OptionParser()
Options.addCommonOptions(parser)
Options.addSEOptions(parser)

parser.add_option('--tcaMode',help = "TCA mode",
                default = 'L_T',
                choices=["L_T","NL_NT","NL_T","L_NT"])
# parser.add_option('--tcaLatency',help = "Set latency for accelerator"
#                 " compute execution latency", default = 8)

(options, args) = parser.parse_args()

if args:
    print("Error: script doesn't take any positional arguments")
    sys.exit(1)

if options.cmd:
    process = Process()
    process.cmd = options.cmd
else:
    print("No workload specified. Exiting!\n", file=sys.stderr)
    sys.exit(1)

# create the system we are going to simulate
system = System()

# Set the clock fequency of the system (and all of its children)
system.clk_domain = SrcClockDomain()
system.clk_domain.clock = '1GHz'
system.clk_domain.voltage_domain = VoltageDomain()

# Set up the system
system.mem_mode = 'timing'               # Use timing accesses
system.mem_ranges = [AddrRange('4096MB')] # Create an address range

# Create a simple CPU
system.cpu = O3_X86_SC_CPU()
system.cpu.tcaMode = options.tcaMode

# David Schlais attempt to change tcaLatency
#system.cpu.fu_pool.opLat = options.tcaLatency

# Create an L1 instruction and data cache
system.cpu.icache = O3_X86_SC_L1ICache(options)
system.cpu.dcache = O3_X86_SC_L1DCache(options)

# Connect the instruction and data caches to the CPU
system.cpu.icache.connectCPU(system.cpu)
system.cpu.dcache.connectCPU(system.cpu)

# Create TLBs for icache and dcache
# When connecting the caches, the clock is also inherited
# from the CPU in question
system.cpu.itb_walker_cache = O3_X86_SC_PageTableWalkerCache(options)
system.cpu.dtb_walker_cache = O3_X86_SC_PageTableWalkerCache(options)
system.cpu.itb.walker.port = system.cpu.itb_walker_cache.cpu_side
system.cpu.dtb.walker.port = system.cpu.dtb_walker_cache.cpu_side
# system.cpu.icache.mem_side = system.cpu.itb_walker_cache.mem_side
# system.cpu.dcache.mem_side = system.cpu.dtb_walker_cache.mem_side

# Create a memory bus, a coherent crossbar, in this case
system.l2bus = L2XBar()

# Hook the CPU ports up to the l2bus
system.cpu.icache.connectBus(system.l2bus)
system.cpu.dcache.connectBus(system.l2bus)
system.cpu.itb_walker_cache.connectBus(system.l2bus)
system.cpu.dtb_walker_cache.connectBus(system.l2bus)

# Create an L2 cache and connect it to the l2bus
system.l2cache = O3_X86_SC_L2Cache(options)
system.l2cache.connectCPUSideBus(system.l2bus)

# Create a memory bus
system.membus = SystemXBar()

# Connect the L2 cache to the membus
system.l2cache.connectMemSideBus(system.membus)

# create the interrupt controller for the CPU
system.cpu.createInterruptController()

# For x86 only, make sure the interrupts are connected to the memory
# Note: these are directly connected to the memory bus and are not cached
if m5.defines.buildEnv['TARGET_ISA'] == "x86":
    system.cpu.interrupts[0].pio = system.membus.master
    system.cpu.interrupts[0].int_master = system.membus.slave
    system.cpu.interrupts[0].int_slave = system.membus.master

# Create a DDR3 memory controller and connect it to the membus
system.mem_ctrl = DDR3_1600_8x8()
system.mem_ctrl.range = system.mem_ranges[0]
system.mem_ctrl.port = system.membus.master

# Connect the system up to the membus
system.system_port = system.membus.slave


# Create a process for a simple "Hello World" application
process = Process()
# Set the command

# cmd is a list which begins with the executable (like argv)
if options.options != "":
    # pargs = options.options.split(';')
    process.cmd = [options.cmd] + options.options.split()
else:
    process.cmd = options.cmd

# Set the cpu to use the process as its workload and create thread contexts
system.cpu.workload = process
system.cpu.createThreads()

root = Root(full_system = False, system = system)
# instantiate all of the objects we've created above
m5.instantiate()

print("Beginning simulation!")
exit_event = m5.simulate()
print('Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause()))

