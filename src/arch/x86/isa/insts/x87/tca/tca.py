# Copyright (c) 2007 The Hewlett-Packard Development Company
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# These files are for TCA NoRF reserach project
#
# Authors: Heng Zhuo

microcode = '''
# from arithmetic/square_root.py
# RF load 4x4
# asm ("fsqrt");
def macroop TFSQRT
{
    myld t2, ds, [0, rcx, rdi]
    myld t2, ds, [1, rcx, rdi]
    myld t2, ds, [2, rcx, rdi]
    myld t2, ds, [3, rcx, rdi]
};

# from arithmetic/round.py
# matrix mul
# asm ("frndint");
def macroop TFRNDINT
{
    mymmul t2, t2, t2
};

# from arithmetic/division.py

# RF store 4x4
# asm ("fdivp");
def macroop TFDIVP
{
    myst t2, ds, [0, r8, rdx]
    myst t2, ds, [1, r8, rdx]
    myst t2, ds, [2, r8, rdx]
    myst t2, ds, [3, r8, rdx]
};

# RF load 2x2
# asm ("fdiv %st(1), %st");
def macroop TFDIV
{
    myld t2, ds, [0, rcx, rdi]
    myld t2, ds, [1, rcx, rdi]
};

# RF store 2x
# asm ("fdiv %st, %st(1)");
def macroop TFDIV2
{
    myst t2, ds, [0, r8, rdx]
    myst t2, ds, [1, r8, rdx]
};

# RF load 8x8
# asm ("fdivr %st(1), %st");
def macroop TFDIVR
{
    myld t2, ds, [0, rcx, rdi]
    myld t2, ds, [1, rcx, rdi]
    myld t2, ds, [2, rcx, rdi]
    myld t2, ds, [3, rcx, rdi]
    myld t2, ds, [4, rcx, rdi]
    myld t2, ds, [5, rcx, rdi]
    myld t2, ds, [6, rcx, rdi]
    myld t2, ds, [7, rcx, rdi]
};

# RF store 8x8
# asm ("fdivr %st, %st(1)");
def macroop TFDIVR2
{
    myst t2, ds, [0, r8, rdx]
    myst t2, ds, [1, r8, rdx]
    myst t2, ds, [2, r8, rdx]
    myst t2, ds, [3, r8, rdx]
    myst t2, ds, [4, r8, rdx]
    myst t2, ds, [5, r8, rdx]
    myst t2, ds, [6, r8, rdx]
    myst t2, ds, [7, r8, rdx]
};


# old 4x4
# asm ("fadd %st, %st(1)");
def macroop TFADD1
{
    myld t2, ds, [0, rcx, rdi]
    myld t2, ds, [1, rcx, rdi]
    myld t2, ds, [2, rcx, rdi]
    myld t2, ds, [3, rcx, rdi]

    myld t2, ds, [0, r8, rsi]
    myld t2, ds, [1, r8, rsi]
    myld t2, ds, [2, r8, rsi]
    myld t2, ds, [3, r8, rsi]

    myld t2, ds, [0, r8, rdx]
    myld t2, ds, [1, r8, rdx]
    myld t2, ds, [2, r8, rdx]
    myld t2, ds, [3, r8, rdx]

    mymmul t2, t2, t2

    myst t2, ds, [0, r8, rdx]
};

# old 2x2
# asm ("fadd %st(1), %st");
def macroop TFADD2
{
    myld t2, ds, [0, rcx, rdi]
    myld t2, ds, [1, rcx, rdi]

    myld t2, ds, [0, r8, rsi]
    myld t2, ds, [1, r8, rsi]

    myld t2, ds, [0, r8, rdx]
    myld t2, ds, [1, r8, rdx]

    mymmul t2, t2, t2

    myst t2, ds, [0, r8, rdx]
};

# old 8x8
# asm ("fsub %st(1), %st");
def macroop TFSUB1
{
    myld t2, ds, [0, rcx, rdi]
    myld t2, ds, [1, rcx, rdi]
    myld t2, ds, [2, rcx, rdi]
    myld t2, ds, [3, rcx, rdi]
    myld t2, ds, [4, rcx, rdi]
    myld t2, ds, [5, rcx, rdi]
    myld t2, ds, [6, rcx, rdi]
    myld t2, ds, [7, rcx, rdi]

    myld t2, ds, [0, r8, rsi]
    myld t2, ds, [1, r8, rsi]
    myld t2, ds, [2, r8, rsi]
    myld t2, ds, [3, r8, rsi]
    myld t2, ds, [4, r8, rsi]
    myld t2, ds, [5, r8, rsi]
    myld t2, ds, [6, r8, rsi]
    myld t2, ds, [7, r8, rsi]

    myld t2, ds, [0, r8, rdx]
    myld t2, ds, [1, r8, rdx]
    myld t2, ds, [2, r8, rdx]
    myld t2, ds, [3, r8, rdx]
    myld t2, ds, [4, r8, rdx]
    myld t2, ds, [5, r8, rdx]
    myld t2, ds, [6, r8, rdx]
    myld t2, ds, [7, r8, rdx]

    mymmul t2, t2, t2

    myst t2, ds, [0, r8, rdx]
};

# place holder
# asm ("fsub %st, %st(1)");
def macroop TFSUB2
{
    mymmul t2, t2, t2
};

# NoRF 2x2
# asm ("fdivrp");
def macroop TFDIVRP
{
    mchecka t2, ds, [0, rcx, rdi]
    mcheckb t2, ds, [0, r8, rsi]
    mcheckc t2, ds, [0, r8, rdx]

    predlda t2, ds, [0, rcx, rdi]
    predlda t2, ds, [1, rcx, rdi]

    predldb t2, ds, [0, r8, rsi]
    predldb t2, ds, [1, r8, rsi]

    predldc t2, ds, [0, r8, rdx]
    predldc t2, ds, [1, r8, rdx]

    mymmul t2, t2, t2

    myst t2, ds, [0, r8, rdx]
};

# NoRF 4x4
# asm ("faddp")
def macroop TFADDP
{
    mchecka t2, ds, [0, rcx, rdi]
    mcheckb t2, ds, [0, r8, rsi]
    mcheckc t2, ds, [0, r8, rdx]

    predlda t2, ds, [0, rcx, rdi]
    predlda t2, ds, [1, rcx, rdi]
    predlda t2, ds, [2, rcx, rdi]
    predlda t2, ds, [3, rcx, rdi]

    predldb t2, ds, [0, r8, rsi]
    predldb t2, ds, [1, r8, rsi]
    predldb t2, ds, [2, r8, rsi]
    predldb t2, ds, [3, r8, rsi]

    predldc t2, ds, [0, r8, rdx]
    predldc t2, ds, [1, r8, rdx]
    predldc t2, ds, [2, r8, rdx]
    predldc t2, ds, [3, r8, rdx]

    mymmul t2, t2, t2

    myst t2, ds, [0, r8, rdx]
};

# NoRF 8x8
# asm ("fsubp")
def macroop TFSUBP
{
    mchecka t2, ds, [0, rcx, rdi]
    mcheckb t2, ds, [0, r8, rsi]
    mcheckc t2, ds, [0, r8, rdx]

    predlda t2, ds, [0, rcx, rdi]
    predlda t2, ds, [1, rcx, rdi]
    predlda t2, ds, [2, rcx, rdi]
    predlda t2, ds, [3, rcx, rdi]
    predlda t2, ds, [4, rcx, rdi]
    predlda t2, ds, [5, rcx, rdi]
    predlda t2, ds, [6, rcx, rdi]
    predlda t2, ds, [7, rcx, rdi]

    predldb t2, ds, [0, r8, rsi]
    predldb t2, ds, [1, r8, rsi]
    predldb t2, ds, [2, r8, rsi]
    predldb t2, ds, [3, r8, rsi]
    predldb t2, ds, [4, r8, rsi]
    predldb t2, ds, [5, r8, rsi]
    predldb t2, ds, [6, r8, rsi]
    predldb t2, ds, [7, r8, rsi]

    predldc t2, ds, [0, r8, rdx]
    predldc t2, ds, [1, r8, rdx]
    predldc t2, ds, [2, r8, rdx]
    predldc t2, ds, [3, r8, rdx]
    predldc t2, ds, [4, r8, rdx]
    predldc t2, ds, [5, r8, rdx]
    predldc t2, ds, [6, r8, rdx]
    predldc t2, ds, [7, r8, rdx]

    mymmul t2, t2, t2

    myst t2, ds, [0, r8, rdx]
};

'''
